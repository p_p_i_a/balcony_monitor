import time
import machine
import struct


### SPI for opc, chip select and opc instance
### (opc cannot share spi with other devices :/)
#
#  Looking into connector cable, not into housing:   
#        _______
#  _____|       |____ 
# | .  .  .  .  .  . |
# | 6  5  4  3  2  2 |
# |__________________|
# 
# functions:         6-GND, 5-CS, 4-MOSI, 3-MISO, 2-SCK, 1-VCC

class OPCN2:
  def __init__(self, spi, CS, oled):
        self.spi = spi
        self.CS = CS
        self.buf = buf = bytearray(1)
        self.oled = oled

  def switch_on(self):
    self.CS.value(0)
    self.spi.readinto(self.buf,0x03)
    time.sleep_ms(10)
    self.spi.readinto(self.buf,0x00)
#    time.sleep_ms(3)
#    self.spi.readinto(self.buf,0x03)
    self.CS.value(1)

  def flush(self):
    self.CS.value(0)
    readedPM = bytearray(13)
    self.spi.readinto(self.buf,0x32)
    time.sleep_ms(9)
    for i in range (0,12):
      self.spi.readinto(self.buf,0x32)
      readedPM[i] = self.buf[0]
      time.sleep_us(10)
    self.CS.value(1)

  def switch_off(self):
    self.CS.value(0)
    self.spi.readinto(self.buf,0x03)
    time.sleep_ms(1)
    self.spi.readinto(self.buf,0x01)
#    time.sleep_ms(3)
#    self.spi.readinto(self.buf,0x00)
    self.CS.value(1)


  def read_values(self):
    self.CS.value(0)
    readedPM = bytearray(13)
    self.spi.readinto(self.buf,0x32)
    time.sleep_ms(9)
    for i in range (0,12):
      self.spi.readinto(self.buf,0x32)
      readedPM[i] = self.buf[0]
      time.sleep_us(10)
    pm1 = (struct.unpack('<f', readedPM[0:4])[0])
    pm25 = (struct.unpack('<f', readedPM[4:8])[0])
    pm10 = (struct.unpack('<f', readedPM[8:12])[0])
    self.CS.value(1)
    return pm1,pm25,pm10



 # def publish_values(self,pm1,pm25,pm10):
 #   print("Measured pm1: " + pm1 + " pm2.5: " + pm25 + " pm10: " + pm10)#
 #
 #   mqtt_feedname = bytes('{:s}/feeds/{:s}'.format(ADAFRUIT_USERNAME, ADAFRUIT_IO_FEEDNAME_PM1), 'utf-8')
 #   client.publish(mqtt_feedname, bytes(str(pm1), 'utf-8'), qos=0) 
 #   mqtt_feedname = bytes('{:s}/feeds/{:s}'.format(ADAFRUIT_USERNAME, ADAFRUIT_IO_FEEDNAME_PM25), 'utf-8')
 #   client.publish(mqtt_feedname, bytes(str(pm25), 'utf-8'), qos=0) 
 #   mqtt_feedname = bytes('{:s}/feeds/{:s}'.format(ADAFRUIT_USERNAME, ADAFRUIT_IO_FEEDNAME_PM10), 'utf-8')
 #   client.publish(mqtt_feedname, bytes(str(pm10), 'utf-8'), qos=0) 
    #client.publish('node/pm1', pm1)
    #client.publish('datastreamers/teplomer_21/teplota', pm25)
    #client.publish('datastreamers/teplomer_22/teplota', pm10)



