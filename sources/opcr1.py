import time
import machine
import struct


### SPI for opc, chip select and opc instance
### (opc cannot share spi with other devices :/)
#
#  Looking into connector cable, not into housing:   
#        _______
#  _____|       |____ 
# | .  .  .  .  .  . |
# | 6  5  4  3  2  2 |
# |__________________|
# 
# functions:         6-GND, 5-CS, 4-MOSI, 3-MISO, 2-SCK, 1-VCC

class OPCR1:
  def __init__(self, spi, CS, oled):
        self.spi = spi
        self.CS = CS
        self.buf = buf = bytearray(1)
        self.oled = oled

  def switch_on(self):
    self.CS.value(0)
    self.spi.readinto(self.buf,0x03)
    time.sleep_ms(10)
    self.spi.readinto(self.buf,0x03)
    time.sleep_ms(3)
    self.spi.readinto(self.buf,0x03)
    self.CS.value(1)

  def flush(self):
    self.CS.value(0)
    readedPM = bytearray(15)
    self.spi.readinto(self.buf,0x32)
    time.sleep_ms(10)
    for i in range (0,15):
      self.spi.readinto(self.buf,0x32)
      readedPM[i] = self.buf[0]
      time.sleep_us(20)
    self.CS.value(1)

  def switch_off(self):
    self.CS.value(0)
    self.spi.readinto(self.buf,0x03)
    time.sleep_ms(10)
    self.spi.readinto(self.buf,0x03)
    time.sleep_ms(3)
    self.spi.readinto(self.buf,0x00)
    self.CS.value(1)


  def read_values(self):
    self.CS.value(0)
    readedPM = bytearray(15)
    self.spi.readinto(self.buf,0x32)
    time.sleep_ms(10)
    for i in range (0,15):
      self.spi.readinto(self.buf,0x32)
      readedPM[i] = self.buf[0]
      time.sleep_us(20)
    A = (struct.unpack('<f', readedPM[1:5])[0])
    B = (struct.unpack('<f', readedPM[5:9])[0])
    C = (struct.unpack('<f', readedPM[9:13])[0])
    self.CS.value(1)
    return A,B,C



 # def publish_values(self,pm1,pm25,pm10):
 #   print("Measured pm1: " + pm1 + " pm2.5: " + pm25 + " pm10: " + pm10)#
 #
 #   mqtt_feedname = bytes('{:s}/feeds/{:s}'.format(ADAFRUIT_USERNAME, ADAFRUIT_IO_FEEDNAME_PM1), 'utf-8')
 #   client.publish(mqtt_feedname, bytes(str(pm1), 'utf-8'), qos=0) 
 #   mqtt_feedname = bytes('{:s}/feeds/{:s}'.format(ADAFRUIT_USERNAME, ADAFRUIT_IO_FEEDNAME_PM25), 'utf-8')
 #   client.publish(mqtt_feedname, bytes(str(pm25), 'utf-8'), qos=0) 
 #   mqtt_feedname = bytes('{:s}/feeds/{:s}'.format(ADAFRUIT_USERNAME, ADAFRUIT_IO_FEEDNAME_PM10), 'utf-8')
 #   client.publish(mqtt_feedname, bytes(str(pm10), 'utf-8'), qos=0) 
    #client.publish('node/pm1', pm1)
    #client.publish('datastreamers/teplomer_21/teplota', pm25)
    #client.publish('datastreamers/teplomer_22/teplota', pm10)



