# Balkon measuring node

Flower monitoring node on my balcony, based on esp32 and micropython. Measuring particles with OPC-N2, BME280, soil humidity (capacitance sensing probe). 
Sending data over WIFI, MQTT protocol. 
(Server side is rpi with mosquitto, grafana, influxdb)

### Dependencies (Debian 10): 

Install Python3, then with pip3:

```sudo pip3 install adafruit-ampy```

```pip3 install esptool```

### Rules 
Create file with rules:

```touch /etc/udev/rules.d/99-esp32.rules ```

Add this line in:

```ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60", MODE="664", GROUP="plugdev"```


reload rules:

```sudo udevadm control --reload-rules && sudo udevadm trigger```

### Preparing the board:

A) esp32:

   ```make erase```

   ```make reflash```

B) esp8266

   ```make erase```

   ```make reflash8266```

The board should be uploaded with micropython and ready to work. you can check it by connecting to serialport, there should be a micropython console


# FW update, flashing: 

Do this after changing the code, then reset the board with "reset" button

```make go```


